package nl.openvalue.nl.docker;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class DockerApplicationTests {

    @Autowired
    private Environment environment;

    @Test
    public void contextLoads() {
        assertThat(environment).isNotNull();
    }


}
